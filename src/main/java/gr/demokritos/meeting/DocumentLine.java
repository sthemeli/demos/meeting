package gr.demokritos.meeting;

public class DocumentLine {
    private String name;
    private String date;
    private String timezone;
    private String availability;

    public DocumentLine() {

    }

    public DocumentLine(String name, String date, String timezone, String availability) {
        this.name = name;
        this.date = date;
        this.timezone = timezone;
        this.availability = availability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }
}
