package gr.demokritos.meeting;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Day {
    private LocalDate date;
    private DayOfWeek dayOfWeek;
    private Set<User> users = new HashSet<>();
    private Map<Timezone, Set<User>> possibleTimezones = new HashMap<>();
    private Map<Timezone, Set<User>> unavailability = new HashMap<>();
    private List<Availability> usersAvailability = new ArrayList<>();

    public Day() {

    }

    public void initPossibleTimezones(Integer duration, Day day) {
        LocalTime startTime = LocalTime.parse("09:00");
        while(startTime.isBefore(LocalTime.parse("21:00"))) {
            LocalTime endTime = startTime.plus(duration, ChronoUnit.HOURS);
            Timezone timezone = new Timezone(day, startTime, endTime);
            possibleTimezones.put(timezone, new HashSet<>());
            unavailability.put(timezone, new HashSet<>());
            startTime = startTime.plus(1, ChronoUnit.HOURS);
        }
    }

    public Day(LocalDate date, DayOfWeek dayOfWeek, Integer duration) {
        this.date = date;
        this.dayOfWeek = dayOfWeek;
        initPossibleTimezones(duration, this);
    }

    public Day(LocalDate date, DayOfWeek dayOfWeek, Set<User> users) {
        this.date = date;
        this.dayOfWeek = dayOfWeek;
        this.users = users;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Map<Timezone, Set<User>> getPossibleTimezones() {
        return possibleTimezones;
    }

    public void setPossibleTimezones(Map<Timezone, Set<User>> possibleTimezones) {
        this.possibleTimezones = possibleTimezones;
    }

    public List<Availability> getUsersAvailability() {
        return usersAvailability;
    }

    public void setUsersAvailability(List<Availability> usersAvailability) {
        this.usersAvailability = usersAvailability;
    }

    public Map<Timezone, Set<User>> getUnavailability() {
        return unavailability;
    }

    public void setUnavailability(Map<Timezone, Set<User>> unavailability) {
        this.unavailability = unavailability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Day day = (Day) o;
        return date.isEqual(day.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date);
    }
}
