package gr.demokritos.meeting;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Calendar {

    public static void main(String[] args) {
        Calendar calendar = new Calendar();
        List<PossibleMeeting> daysOfPossibleMeeting = new ArrayList<>();
        try {
            Scanner scanner = new Scanner( System.in );
            System.out.println("Give meeting's duration in Integer");
            String duration = scanner.nextLine();
            scanner = new Scanner( System.in );
            System.out.println("Give meeting's name");
            String meetingName = scanner.nextLine();
            Week week = calendar.readDataFile(Integer.parseInt(duration), meetingName);
            week.getWorkingWeek().forEach(day -> {
                day.getUsersAvailability().forEach(availability -> findAvailableAndUnavailableUsers(day, availability));
                List<Pair<Timezone, Set<User>>> possibleMeetingTimezones= findTimezoneWithMostAttendingUsersForSpecificDay(day);
                if(!CollectionUtils.isEmpty(possibleMeetingTimezones)) {
                    possibleMeetingTimezones.forEach(selectedTuple -> {
                        if(selectedTuple!=null && selectedTuple.getLeft()!=null && selectedTuple.getRight()!=null) {
                            PossibleMeeting possibleMeeting = createPossibleMeetingForSpecificDay(day, selectedTuple);
                            daysOfPossibleMeeting.add(possibleMeeting);
                        }
                    });
                }
            });
            printPossibleMeetingsData(daysOfPossibleMeeting);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private Week readDataFile(Integer duration, String meetingName) throws URISyntaxException {
        Path path = Paths.get("src/main/resources/" + meetingName + ".txt");
        Set<User> users = new HashSet<>();
        Week week = getSpecificWeek(duration);
        try (Stream<String> lines = Files.lines(path)) {
            for(String line : lines.collect(Collectors.toList())){
                List<String> lineParts = Arrays.asList(line.split(","));
                User user = new User(lineParts.get(0).trim());
                users.add(user);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                LocalDate date = LocalDate.parse(lineParts.get(1).trim(), formatter);
                Day selectedDay = findDayOfWeek(week, date);
                if(selectedDay!=null) {
                    initAvailabilityForLineData(lineParts, user, selectedDay);
                }
            }
            week.setUsers(users);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return week;
    }

    private Week getSpecificWeek(Integer duration) {
        Scanner scanner = new Scanner( System.in );
        System.out.println("Press y if you run the file for this week, or n if it is about next week");
        String input = scanner.nextLine();
        Week week;
        if(input.equalsIgnoreCase("y")) {
            week = initWeek(duration, LocalDate.now());
        } else {
            scanner = new Scanner(System.in);
            System.out.println("Give the date of next Monday in dd-MM-yyyy format");
            input = scanner.nextLine();
            week = initWeek(duration, LocalDate.parse(input, DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        }
        return week;
    }

    private void initAvailabilityForLineData(List<String> lineParts, User user, Day selectedDay) {
        Availability availability = new Availability();
        availability.setUser(user);
        availability.setDay(selectedDay);
        Timezone timezone = parseTimezoneString(lineParts.get(2).trim(), selectedDay.getPossibleTimezones().keySet());
        timezone.setDay(selectedDay);
        availability.setTimezone(timezone);
        availability.setAvailable(Boolean.valueOf(lineParts.get(3).trim()));
        selectedDay.getUsersAvailability().add(availability);
    }

    private Week initWeek(Integer duration, LocalDate date) {
        Week currentWeek = new Week();
        currentWeek.initWeek(date, duration);
        return currentWeek;
    }

    private Day findDayOfWeek(Week week, LocalDate date) {
        if(week.getMonday().getDate().isEqual(date)) {
            return week.getMonday();
        } else if(week.getTuesday().getDate().isEqual(date)) {
            return week.getTuesday();
        } else if(week.getWednesday().getDate().isEqual(date)) {
            return week.getWednesday();
        } else if(week.getThursday().getDate().isEqual(date)) {
            return week.getTuesday();
        } else if(week.getFriday().getDate().isEqual(date)) {
            return week.getFriday();
        } else {
            return null;
        }
    }

    private Timezone parseTimezoneString(String tz, Set<Timezone> timezones) {
        List<String> parts = Arrays.asList(tz.split("-"));
        LocalTime startTime = LocalTime.parse(parts.get(0));
        LocalTime endTime = LocalTime.parse(parts.get(1));
        for(Timezone timezone : timezones) {
            if(timezone.getStartTime().equals(startTime) && timezone.getEndTime().equals(endTime)) {
                return timezone;
            }
        }
        return new Timezone(startTime, endTime);
    }

    private static void findAvailableAndUnavailableUsers(Day day, Availability availability) {
        if(availability.getAvailable()) {
            if (!day.getPossibleTimezones().containsKey(availability.getTimezone())) {
                day.getPossibleTimezones().put(availability.getTimezone(), new HashSet<>());
            }
            day.getPossibleTimezones().get(availability.getTimezone()).add(availability.getUser());
        } else {
            if (!day.getUnavailability().containsKey(availability.getTimezone())) {
                day.getUnavailability().put(availability.getTimezone(), new HashSet<>());
            }
            day.getUnavailability().get(availability.getTimezone()).add(availability.getUser());
        }
    }

    private static List<Pair<Timezone, Set<User>>> findTimezoneWithMostAttendingUsersForSpecificDay(Day day) {
        int max = 0;
        List<Pair<Timezone, Set<User>>> possibleTimezones = new ArrayList<>();
        Pair<Timezone, Set<User>> selectedTuple = null;
        for(Timezone timezone : day.getPossibleTimezones().keySet()) {
            int countAvailable = day.getPossibleTimezones().get(timezone).size();
            int countUnavailable = day.getUnavailability().get(timezone).size();
            if(countAvailable >= max) {
                max = countAvailable;
                if(max!=0) {
                    selectedTuple = Pair.of(timezone, day.getPossibleTimezones().get(timezone));
                    possibleTimezones.add(selectedTuple);
                }
            }
        }
        Comparator<Pair<Timezone, Set<User>>> comparator = Comparator.comparingInt(o -> o.getLeft().getStartTime().getHour());
        possibleTimezones.sort(comparator);
        return possibleTimezones;
    }

    private static PossibleMeeting createPossibleMeetingForSpecificDay(Day day, Pair<Timezone, Set<User>> selectedTuple) {
        PossibleMeeting possibleMeeting = new PossibleMeeting();
        possibleMeeting.setDay(day);
        possibleMeeting.setTimezone(selectedTuple.getLeft());
        possibleMeeting.setAttendingUsers(selectedTuple.getRight());
        possibleMeeting.setNotAttendingUsers(day.getUnavailability().get(selectedTuple.getLeft()));
        return possibleMeeting;
    }

    private static void printPossibleMeetingsData(List<PossibleMeeting> daysOfPossibleMeeting) {
        Collections.sort(daysOfPossibleMeeting);
        for(PossibleMeeting possibleMeeting : daysOfPossibleMeeting) {
            System.out.println("### Day " + possibleMeeting.getDay().getDayOfWeek().toString() + " and start time " +
                    possibleMeeting.getTimezone().getStartTime().toString() + " and end time " +
                    possibleMeeting.getTimezone().getEndTime().toString() + " number of members " +
                            possibleMeeting.getAttendingUsers().size() + "\n Members to attend ");
            for(User user : possibleMeeting.getAttendingUsers()) {
                System.out.println(user.getName());
            }
            System.out.println("Not attending members: ");
            for(User user : possibleMeeting.getNotAttendingUsers()) {
                System.out.println(user.getName());
            }
        }
    }
}
